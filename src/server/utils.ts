/* istanbul ignore file */
import fs from 'fs-extra';
import prettyBytes from 'pretty-bytes';
import logger from '../logger/logger';

export const fsEmptyDir = (path: string): void => {
  fs.emptyDirSync(path);
};

export const fsRemove = (path: string): void => {
  fs.removeSync(path);
  logger.utils.info('Object was removed', { url: path });
};

export const fsFileSize = (path: string): string => {
  const stats = fs.statSync(path);

  return prettyBytes(stats.size);
};
