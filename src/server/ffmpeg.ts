import path from 'path';
import fs from 'fs-extra';
import partial from 'lodash/partial';
import ffmpeg from 'ffmpeg';
import { exec } from 'child_process';
import { TaskError } from './errors';
import { fsFileSize } from './utils';
import { TVideoMetadata } from '../types/app';
import logger from '../logger/logger';
import { ELogErrorCode } from '../logger/ELogErrorCode';

type TExecFunction = (cwd: string) => Promise<void>;

const FFMPEG_CONFIG_FILE_NAME = 'list.txt';

const createFFMPEGConfigFile = (dir: string, paths: string[]): Promise<void> => {
  const configFilePath = path.resolve(dir, FFMPEG_CONFIG_FILE_NAME);
  const listContent = paths.map((filePath) => `file '${path.basename(filePath)}'`).join('\n');

  return fs.outputFile(configFilePath, listContent);
};

/**
 * @param {string} cwd - home directory
 * @param {string} cmd - cmd(sh/cmd) command
 * @returns {Promise<void>}
 */
const execCommand = (cwd: string, cmd: string): Promise<void> =>
  new Promise((resolve, reject) =>
    exec(cmd, { cwd }, (error, stdout, stderr) => {
      if (error || stderr) {
        logger.ffmpeg.error(ELogErrorCode.FfmpegProcessing, 'Command was executed with errors', {
          cmd,
          stdout,
          stderr,
        });
        reject(new TaskError(stderr, true));

        return;
      } else {
        logger.ffmpeg.info('Command was executed', { cmd, stdout });
      }

      resolve();
    })
  );

const validateVideoFiles = (exec: TExecFunction, paths: string[]): Promise<void> =>
  void Promise.all(paths.map((filePath) => exec(`ffprobe -v error ${path.basename(filePath)}`)));

const concatAndConvertVideoFiles = (exec: TExecFunction, output: string): Promise<void> =>
  exec(`ffmpeg -v error -f concat -i ${FFMPEG_CONFIG_FILE_NAME} -c copy "${output.replace(/"/g, '\\"')}"`);

/**
 * Converts all files from the dir, if there is exists any corrupted file then cancel converting,
 * because ffmpeg skips corrupted files
 * @param {string} dir - place where the function looking video files
 * @param {string} output - result file name
 * @returns {Promise<void>}
 */
export const convertVideoFilesFromDir = async (dir: string, output: string): Promise<void> => {
  const exec = partial(execCommand, dir) as TExecFunction;
  const fileNames: string[] = await fs.readdir(dir);
  const filePaths: string[] = fileNames.map((filename) => path.resolve(dir, filename));

  await validateVideoFiles(exec, filePaths);
  await createFFMPEGConfigFile(dir, filePaths);
  await concatAndConvertVideoFiles(exec, output);
};

type TFfmpegVideoMetadata = {
  video: {
    resolution: {
      w: number;
      h: number;
    };
  };
  duration: {
    raw: string;
  };
};

export const getVideoMetadata = async (url: string): Promise<TVideoMetadata> => {
  const metadata: TVideoMetadata = {
    resolution: '',
    duration: '',
    size: '',
  };

  metadata.size = fsFileSize(url);

  const video = await new ffmpeg(url);
  const vmd = (video.metadata as unknown) as TFfmpegVideoMetadata;
  const { w, h } = vmd.video.resolution;

  metadata.resolution = `${w}x${h}`;
  metadata.duration = vmd.duration.raw.split('.')[0];

  return metadata;
};
