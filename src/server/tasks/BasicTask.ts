import { EventEmitter } from 'events';
import { TTaskBody } from '@s2m/cloud';
import { IProcessingConfig } from 'env-config';
import { TaskError } from '../errors';

export enum TaskEventName {
  COMPLETE = 'complete',
  ERROR = 'error',
}

export default abstract class Task extends EventEmitter {
  constructor(protected _desc: TTaskBody) {
    super();
  }

  public abstract run(cfg: IProcessingConfig): Promise<void>;

  public get desc(): TTaskBody {
    return this._desc;
  }

  onComplete(listener: (result: TTaskBody) => void): void {
    this.on(TaskEventName.COMPLETE, listener);
  }

  onError(listener: (error: TaskError) => void): void {
    this.on(TaskEventName.ERROR, listener);
  }
}
