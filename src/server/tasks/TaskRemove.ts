import BasicTask from './BasicTask';
import { IProcessingConfig } from 'env-config';
import { getStorage, TTaskBody } from '@s2m/cloud';
import { TaskEventName } from './BasicTask';
import logger from '../../logger/logger';

export type TOnCompleteTaskRemove = TTaskBody;

export default class TaskRemove extends BasicTask {
  constructor(desc: TTaskBody) {
    super(desc);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async run(cfg: IProcessingConfig): Promise<void> {
    const { src } = this.desc;

    try {
      logger.task.info('Removal task was started', { src });

      await getStorage().removeFromS3File(src);

      logger.task.info('Removal task was ended', { src });

      this.emit(TaskEventName.COMPLETE, {
        ...this.desc,
      } as TOnCompleteTaskRemove);
    } catch (e) {
      this.emit(TaskEventName.ERROR, e);
    }
  }
}
