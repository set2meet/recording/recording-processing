import path from 'path';
import BasicTask from './BasicTask';
import { convertVideoFilesFromDir, getVideoMetadata } from '../ffmpeg';
import { TVideoMetadata } from '../../types/app';
import { getStorage, TTaskBody } from '@s2m/cloud';
import { fsEmptyDir, fsRemove } from '../utils';
import { TaskEventName } from './BasicTask';
import logger from '../../logger/logger';
import { IProcessingConfig } from 'env-config';

export type TOnCompleteTaskCreate = TTaskBody & {
  duration: string;
  resolution: string;
  size: string;
  type: string;
};

export default class TaskCreate extends BasicTask {
  private readonly type: string;
  private resolution: string;
  private duration: string;
  private size: string;
  private src: string;

  constructor(desc: TTaskBody) {
    super(desc);

    this.type = (this.desc.type || 'mp4') as string;
  }

  public async run(cfg: IProcessingConfig): Promise<void> {
    const dir = cfg.workingDir;
    const { id, src } = this.desc;

    const workingDir = path.resolve(dir, id);
    const mergedFile = path.resolve(dir, `${id}.mp4`);
    const outputFile = path.resolve(dir, `${id}.${this.type}`);

    try {
      logger.task.info('Creation task was started', { workingDir, outputFile });

      fsEmptyDir(workingDir);

      await getStorage().downloadFromS3Dir(src, workingDir);
      await convertVideoFilesFromDir(workingDir, outputFile);

      await this.saveResultingVideo(outputFile);

      await this.saveVideoMetadata(outputFile);

      this.cleanup(workingDir, mergedFile, outputFile);
      await getStorage().removeFromS3Dir(src); // check this!

      logger.task.info('Creation task was ended', { workingDir, outputFile });

      this.emit(TaskEventName.COMPLETE, {
        id: this.desc.id,
        action: this.desc.action,
        src: this.src,
        duration: this.duration,
        resolution: this.resolution,
        size: this.size,
        type: this.type,
      } as TOnCompleteTaskCreate);
    } catch (e) {
      this.emit(TaskEventName.ERROR, e);
    }
  }

  private cleanup(workingDir: string, mergedFile: string, outputFile: string): void {
    fsRemove(workingDir);
    fsRemove(mergedFile);
    fsRemove(outputFile);
  }

  private async saveResultingVideo(outputFile: string): Promise<void> {
    const metadata = {
      type: this.type,
    };

    const s3Key = await getStorage().uploadRecordingProcessingVideo(outputFile, metadata);

    this.src = s3Key;
  }

  private async saveVideoMetadata(outputFile: string): Promise<void> {
    const metadata = await getVideoMetadata(outputFile);

    this.saveOutputMetadata(metadata);
  }

  private saveOutputMetadata(meta: TVideoMetadata): void {
    this.resolution = meta.resolution;
    this.duration = meta.duration;
    this.size = meta.size;

    const metadata = { src: this.src, ...meta };

    logger.task.info('Output metadata was saved', { metadata });
  }
}
