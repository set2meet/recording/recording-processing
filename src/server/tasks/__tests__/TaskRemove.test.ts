import { mock } from 'jest-mock-extended';
import { TaskActionType, IStorage, getStorage } from '@s2m/cloud';
import { TaskEventName } from '../BasicTask';
import TaskRemove from '../TaskRemove';

jest.mock('@s2m/cloud');

describe('TaskRemove', () => {
  const mockStorage = mock<IStorage>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();

    (getStorage as jest.Mock).mockImplementation(() => mockStorage);
  });

  test('works properly', async () => {
    const task = new TaskRemove({
      id: 'some id',
      src: 'some src',
      action: TaskActionType.Remove,
    });
    let complete = false;

    task.on(TaskEventName.COMPLETE, () => (complete = true));

    await task.run({
      workingDir: 'work dir',
      searchTimeout: 123,
      nextTaskTimeout: 456,
      waitTimeout: 1,
      visibilityTimeout: 2,
    });

    expect(complete).toBeTruthy();
    // eslint-disable-next-line @typescript-eslint/unbound-method
    expect(mockStorage.removeFromS3File).toHaveBeenCalled();
  });
});
