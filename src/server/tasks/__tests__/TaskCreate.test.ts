import path from 'path';
import fs from 'fs-extra';
import { mock } from 'jest-mock-extended';
import { TaskActionType, IStorage, getStorage } from '@s2m/cloud';
import { fsEmptyDir, fsRemove, fsFileSize } from '../../utils';
import TaskCreate, { TOnCompleteTaskCreate } from '../TaskCreate';

const TEST_TIMEOUT = 30000;
const TASK_ID = 'anId';
const WORKING_DIR = './testVideo';

jest.mock('@s2m/cloud');
jest.mock('../../utils');

describe('TaskCreate', () => {
  const mockStorage = mock<IStorage>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();

    (getStorage as jest.Mock).mockImplementation(() => mockStorage);
    (fsFileSize as jest.Mock).mockReturnValue('123 Kb');
  });

  test(
    'works properly',
    async () => {
      let initialized: string = null;
      const removed = new Set<string>();

      (fsEmptyDir as jest.Mock).mockImplementation((p) => {
        initialized = p as string;
      });
      (fsRemove as jest.Mock).mockImplementation((p) => {
        removed.add(p);
      });

      const task = new TaskCreate({
        id: TASK_ID,
        src: 'anSrc',
        action: TaskActionType.Create,
      });

      let result: TOnCompleteTaskCreate = null;

      task.onComplete((evt) => {
        result = evt as TOnCompleteTaskCreate;
      });

      await task.run({
        workingDir: WORKING_DIR,
        searchTimeout: 123,
        nextTaskTimeout: 456,
        visibilityTimeout: 1,
        waitTimeout: 2,
      });

      // cleanup
      fs.removeSync(path.join(WORKING_DIR, TASK_ID, 'list.txt'));
      removed.forEach((path) => {
        if (fs.existsSync(path) && fs.lstatSync(path).isFile()) {
          fs.removeSync(path);
        }
      });

      expect(initialized).toEqual(path.resolve(path.join(WORKING_DIR, TASK_ID)));
      expect(removed.has(initialized)).toBeTruthy();
      expect(removed.has(path.resolve(WORKING_DIR, TASK_ID + '.mp4'))).toBeTruthy();

      // eslint-disable-next-line @typescript-eslint/unbound-method
      expect(mockStorage.downloadFromS3Dir).toHaveBeenCalled();
      // eslint-disable-next-line @typescript-eslint/unbound-method
      expect(mockStorage.uploadRecordingProcessingVideo).toHaveBeenCalled();
      // eslint-disable-next-line @typescript-eslint/unbound-method
      expect(mockStorage.removeFromS3Dir).toHaveBeenCalled();

      expect(result).not.toBeNull();
      expect(result.id).toEqual(TASK_ID);
      expect(result.action).toEqual(TaskActionType.Create);
      expect(result.type).toEqual('mp4');
      expect(result.duration).toBeDefined();
      expect(result.resolution).toBeDefined();
      expect(result.size).toBeDefined();
    },
    TEST_TIMEOUT
  );

  test('fails', async () => {
    mockStorage.downloadFromS3Dir.mockImplementation(() => {
      throw new Error('test error');
    });

    const task = new TaskCreate({
      id: TASK_ID,
      src: 'anSrc',
      action: TaskActionType.Create,
    });

    let complete = false;
    let failed = false;

    task.onComplete(() => {
      complete = true;
    });
    task.onError(() => {
      failed = true;
    });

    await task.run({
      workingDir: WORKING_DIR,
      searchTimeout: 123,
      nextTaskTimeout: 456,
      visibilityTimeout: 1,
      waitTimeout: 2,
    });

    expect(complete).toBeFalsy();
    expect(failed).toBeTruthy();
  });
});
