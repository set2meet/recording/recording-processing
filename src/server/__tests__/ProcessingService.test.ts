import { mock } from 'jest-mock-extended';
import { IProcessingConfig } from 'env-config';
import {
  TTaskBody,
  IQueue,
  getQueue,
  IConsumer,
  TTaskListener,
  TaskActionType,
  IStorage,
  getStorage,
} from '@s2m/cloud';
import { TaskError } from '../errors';
import ProcessingService from '../ProcessingService';
import { convertVideoFilesFromDir, getVideoMetadata } from '../ffmpeg';
import IDistributedLock from '@s2m/recording-db/lib/IDistributedLock';

/* eslint-disable @typescript-eslint/unbound-method */

const BIT_PAUSE = 100;

jest.mock('@s2m/cloud');

const mockDistributedLock = mock<IDistributedLock>();

jest.mock('@s2m/recording-db', () => ({
  getDistributedLock: () => mockDistributedLock,
}));

jest.mock('../utils');
jest.mock('../ffmpeg');

const pause = async (): Promise<void> => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, BIT_PAUSE);
  });
};

describe('processing', () => {
  const mockStorage = mock<IStorage>();

  const config: IProcessingConfig = {
    workingDir: './test',
    searchTimeout: 123,
    nextTaskTimeout: 456,
    waitTimeout: 1,
    visibilityTimeout: 2,
  };

  let queueListener: TTaskListener = null;
  let queuePromise: Promise<void> = null;
  const consumerMock: IConsumer = {
    emitTask: (desc: TTaskBody): void => {
      if (queueListener) {
        let taskPromiseResolve: () => void = null;

        queuePromise = new Promise<void>((resolve) => {
          taskPromiseResolve = resolve;
        });

        queueListener({
          desc,
          done: (): Promise<void> => {
            taskPromiseResolve();

            return Promise.resolve();
          },
          break: (): Promise<void> => {
            taskPromiseResolve();

            return Promise.resolve();
          },
        });
      }
    },
    onTask: (listener: TTaskListener): void => {
      if (queueListener) {
        throw new Error('this test code allows only one listener');
      }
      queueListener = listener;
    },
  };

  const queueMock = mock<IQueue>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();

    queueListener = null;
    queuePromise = null;

    (getStorage as jest.Mock).mockImplementation(() => mockStorage);
    queueMock.getConsumerForTask.mockReturnValue(consumerMock);
    (getQueue as jest.Mock).mockReturnValue(queueMock);
    (getVideoMetadata as jest.Mock).mockReturnValue({
      resolution: '640x480',
      duration: '222',
      size: '333',
    });
  });

  test('works properly', async () => {
    mockDistributedLock.acquireLock.mockResolvedValue(true);
    mockStorage.uploadRecordingProcessingVideo.mockResolvedValue('src1');

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    new ProcessingService(config);

    consumerMock.emitTask({
      id: 'id',
      src: 'src',
      action: TaskActionType.Create,
    });

    await queuePromise;
    await pause();

    expect(queueMock.createRecordReport as jest.Mock).toBeCalledWith({
      action: 'create',
      duration: '222',
      resolution: '640x480',
      size: '333',
      id: 'id',
      src: 'src1',
      type: 'mp4',
    });
  });

  test('works properly with acquireLock = false', async () => {
    mockDistributedLock.acquireLock.mockResolvedValue(false);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    new ProcessingService(config);

    consumerMock.emitTask({
      id: 'id',
      src: 'src',
      action: TaskActionType.Create,
    });

    await queuePromise;
    await pause();

    expect(queueMock.createRecordReport as jest.Mock).not.toBeCalled();
  });

  test('fatal', async () => {
    mockDistributedLock.acquireLock.mockResolvedValue(true);
    (convertVideoFilesFromDir as jest.Mock).mockImplementation(() => {
      throw new TaskError('test error', true);
    });

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    new ProcessingService(config);

    consumerMock.emitTask({
      id: 'id',
      src: 'src',
      action: TaskActionType.Create,
    });

    await queuePromise;
    await pause();

    expect(queueMock.createRecordReport).toBeCalledWith({
      action: 'create',
      error: 'test error',
      id: 'id',
      src: 'src',
    });
  });

  test('non fatal', async () => {
    mockDistributedLock.acquireLock.mockResolvedValue(true);
    (convertVideoFilesFromDir as jest.Mock).mockImplementation(() => {
      throw new TaskError('test error', false);
    });

    new ProcessingService(config);

    consumerMock.emitTask({
      id: 'id',
      src: 'src',
      action: TaskActionType.Create,
    });

    await queuePromise;
    await pause();

    expect(mockDistributedLock.releaseLock).toBeCalledWith('id');
  });
});
