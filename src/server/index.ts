/* istanbul ignore file */
import ProcessingService from './ProcessingService';
import http from 'http';
import Koa, { Context } from 'koa';
import Router from 'koa-router';
import logger from '../logger/logger';
import { IProcessingConfig, IRecordingProcessingConfig, readConfig } from 'env-config';

const PORT = 3001;

const app = new Koa<Context>();
const router = new Router();

router.get('/health', (ctx: Context) => (ctx.body = 'ok'));

app.use(router.routes());

const createService = async () => {
  let config = (await readConfig<IRecordingProcessingConfig>()).recordingProcessing;
  const defaultProcessingConfig: IProcessingConfig = {
    workingDir: 'tmp',
    searchTimeout: 10000, // sec
    nextTaskTimeout: 1000, // sec
    visibilityTimeout: 6000, // sec
    waitTimeout: 10, // sec
  };

  config = Object.assign({}, defaultProcessingConfig, config);

  if (!config) {
    process.exit(1);
  }

  logger.service.info('Service was started');

  // run processing service
  return new ProcessingService(config);
};

// eslint-disable-next-line @typescript-eslint/no-misused-promises
http.createServer(app.callback()).listen(PORT, () => {
  logger.service.info(`Server started on ${PORT}`, { PORT });

  void createService();
});
