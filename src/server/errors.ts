/* istanbul ignore file */
export class TaskError extends Error {
  private _fatal: boolean;

  constructor(msg: string, fatal = false) {
    super(msg);

    this._fatal = fatal;
  }

  public get fatal(): boolean {
    return this._fatal;
  }
}
