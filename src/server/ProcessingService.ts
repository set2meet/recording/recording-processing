import { IProcessingConfig } from 'env-config';
import { getQueue, TQueueTask, IConsumer, TTaskBody } from '@s2m/cloud';
import { TaskError } from './errors';
import logger from '../logger/logger';
import { ELogErrorCode } from '../logger/ELogErrorCode';
import TaskCreate from './tasks/TaskCreate';
import TaskRemove from './tasks/TaskRemove';
import Task from './tasks/BasicTask';
import { getDistributedLock } from '@s2m/recording-db';

const Tasks = {
  create: TaskCreate,
  remove: TaskRemove,
};

type TAction = keyof typeof Tasks;

enum LogNames {
  processTask = 'processTask',
  completeTask = 'completeTask',
  endTask = 'endTask',
  breakTask = 'breakTask',
  completeTaskWithError = 'completeTaskWithError',
}

export default class ProcessingService {
  private task: Task;
  private queueTask: TQueueTask;
  private config: IProcessingConfig;
  private consumer: IConsumer;

  constructor(config: IProcessingConfig) {
    this.config = config;

    this.consumer = getQueue().getConsumerForTask({
      nextTaskTimeout: config.nextTaskTimeout,
      searchTaskTimeout: config.searchTimeout,
      waitTimeout: config.waitTimeout,
      visibilityTimeout: config.visibilityTimeout,
    });

    //eslint-disable-next-line @typescript-eslint/no-misused-promises
    this.consumer.onTask(this.processTask);
  }

  private processTask = async (task: TQueueTask): Promise<void> => {
    const action: TAction = task.desc.action;
    const recordId = task.desc.id;
    const isLockAdded = await getDistributedLock().acquireLock(recordId, this.config.waitTimeout);

    this.queueTask = task;

    if (isLockAdded) {
      logger.service.info('Task processing was started', { task });

      this.task = new Tasks[action](task.desc);

      this.task.onComplete((data) => {
        logger.service.debug('onComplete task', { recordId });

        void this.completeTask(data);
      });

      this.task.onError((err) => {
        logger.service.debug('onError task', { recordId });

        void this.breakTask(err);
      });

      void this.task.run(this.config);
    } else {
      await this.queueTask.break();
    }
  };

  private async completeTask(data: TTaskBody): Promise<void> {
    logger.service.info('Task completing was started', { data });

    try {
      await getQueue().createRecordReport({ ...data });

      await this.queueTask.done();
    } catch (e) {
      logger.service.error(ELogErrorCode.TaskExecution, 'Task was completed with error', {
        data: this.task.desc,
        error: (e as Error).message,
      });
    }
  }

  private async breakTask(err: TaskError): Promise<void> {
    logger.service.error(ELogErrorCode.TaskExecution, 'Task was interrupted', {
      data: this.task.desc,
      error: err.message,
      fatal: err.fatal,
    });

    if (err.fatal) {
      await this.endTask(err);
    } else {
      await this.queueTask.break();

      await getDistributedLock().releaseLock(this.task.desc.id);
    }
  }

  private async endTask(err: TaskError): Promise<void> {
    const { desc } = this.queueTask;

    try {
      await getQueue().createRecordReport({ ...desc, error: err.message });

      await this.queueTask.done();
    } catch (e) {
      logger.service.error(ELogErrorCode.TaskExecution, 'Task was ended with error', {
        data: this.task.desc,
        error: (e as Error).message,
      });
    }
  }
}
