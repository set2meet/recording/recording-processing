export type TVideoMetadata = {
  duration: string;
  resolution: string;
  size: string;
};
