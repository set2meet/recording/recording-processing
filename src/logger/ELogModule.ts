export enum ELogModule {
  Service = 'Service',
  Task = 'Task',
  Ffmpeg = 'Ffmpeg',
  Utils = 'Utils',
  Config = 'Config',
}
