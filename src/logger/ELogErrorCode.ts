export enum ELogErrorCode {
  ServiceExecution = 'E_SERVICE_EXECUTION',
  TaskExecution = 'E_TASK_EXECUTION',
  FfmpegProcessing = 'E_FFMPEG_PROCESSING',
  ConfigProcessing = 'E_CONFIG_PROCESSING',
}
