/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { ServerLogger, S2MServices } from '@s2m/logger';
import { ELogErrorCode } from './ELogErrorCode';
import { ELogModule } from './ELogModule';

const appLogger: ServerLogger<ELogModule, ELogErrorCode> = new ServerLogger(
  S2MServices.RecordingProcessing
);

const serviceLogger = appLogger.createBoundChild(ELogModule.Service);
const taskLogger = appLogger.createBoundChild(ELogModule.Task);
const ffmpegLogger = appLogger.createBoundChild(ELogModule.Ffmpeg);
const configLogger = appLogger.createBoundChild(ELogModule.Config);
const utilsLogger = appLogger.createBoundChild(ELogModule.Utils);

export default {
  service: serviceLogger,
  task: taskLogger,
  ffmpeg: ffmpegLogger,
  config: configLogger,
  utils: utilsLogger,
};
