## Recording processing

Service for processing tasks from the queue for creating and deleting media files

- On task with action `create` merge and convert mp4 to mp4, upload to s3
- On task with action `remove` remove pointed file from s3

#### Setup

1. see virtual-room readme
2. run recording-processing locally
   1. Run `npm i`
   2. Install ffmpeg ( https://ffmpeg.org/download.html )
   3. `npm run server:dev`

#### Development

Run local recording-processing service:

- `npm run server:dev:run` - just run the server with out watching changes in local dependencies
- `npm run server:dev` - run the server with watching changes in local dependencies

#### Config

Config has next structure:

```json
{
  "s3c": {
    "credentials": {
      "key": "***",
      "secret": "***"
    },
    "destination": {
      "bucket": "s2m-app-recording",
      "target": {
        "draft": "dev-dump",
        "final": "dev-output"
      }
    }
  },
  "sqs": {
    "credentials": {
      "key": "***",
      "secret": "***"
    },
    "destination": {
      "group": "recording",
      "region": "us-east-1",
      "queue": {
        "task": "***",
        "report": "***"
      }
    }
  },
  "redisConfig": {
    "port": 6379,
    "host": "127.0.0.1"
  },
  "recordingProcessing": {
    "workingDir": "testVideo",
    "nextTaskTimeout": 1001,
    "searchTimeout": 10001,
    "waitTimeout": 10
  }
}
```

```

| field name      |  type  | required | default | description                                                                                                                                |
| --------------- | :----: | :------: | ------: | ------------------------------------------------------------------------------------------------------------------------------------------ |
| workingDir      | string |    -     |     tmp | the path to the working directory where draft files will be downloaded and other manipulations are performed to obtain the finished result |
| nextTaskTimeout | number |    -     |    1000 | timeout between processing tasks (in ms)                                                                                                   |
| searchTimeout   | number |    -     |   10000 | timeout between queue checks for tasks                                                                                                     |
| waitTimeout     | number |    -     |         | pause if error happened task processing-service will try reprocess in after waitTimeout ms
| redisConfig     |        |          |         | uses as distributed lock between recording-processing services
| s3c.destination.target.draft                  | place where recroding-capture save draft files and recroding-processing gets the files for processing (concatenation/converting)
| s3c.destination.target.final                  | recroding-processing saves the files after (concatenation/converting)
| sqs.destination.target.task                   | query - initiator of processing 
| sqs.destination.target.report                 | query - result of processing 
```

#### Build with Docker

##### Requirements:

- Install Docker version 18.0+
- In order to use ssh-key mapping inside docker you should enable buildkit and experemental features using your daemon.json:

```json
{
  "experimental": true,
  "features": {
    "buildkit": true
  }
}
```

##### Build
see `s2m-infra` - docker based s2m project with all necessaries services

#### Modules

##### ffmpeg

Utilities to work with media files

Exports:

- **convertVideoFile**: (input: string, output: string) => Promise&lt;void&gt;

Merges and Converts input file with dir `input` to file `output`

- **getVideoMetadata**: (url: string) => Promise&lt;VideoMetadata&gt;

Return metadata for file by its url, where metadata has next fields

| field      |  type  | example  | description                |
| ---------- | :----: | -------- | -------------------------- |
| duration   | string | 01:23:14 | duration of the media file |
| resolution | number | 800x600  | width x height             |
| size       | number | 345 Mb   | size of the file           |

### Docker build params

For running this service through Docker you should provide options for env-config:
`-module {module-name} -stand {stand-name} -path {output-config-path} -url {hashicorp-url} -namespace {hashicorp-namespace} -role_id {CI-role-id} -secret_id {CI-secret-id} -npmScripts {script1, script2,...}`
where:
- `{module-name}`: name of the service (module) for which we want to build the config, without brackets
- `{stand-name}`: name of the stand for which we want config will be use, without brackets, or use `local` for local development
- `{output-config-path}`: path for your config
- `{CI-role-id}`: This attribute is for production/dev stands. This has been using inside CI/CD pipeline.
- `{CI-secret-id}`: This attribute is for production/dev stands. This has been using inside CI/CD pipeline.
- `{hashicorp-url}`: For custom hashicorp url.
- `{hashicorp-namespace}`: For custom hashicorp namespace.
