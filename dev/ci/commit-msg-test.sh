#!/bin/sh 
if [ "$(which perl > /dev/null; echo $?)" != "0" ]; then
	echo -e "\e[31mYou need to install perl\e[0m"
	exit 1
fi

if [ "$#" -lt 1 ]; then
	echo -e "\e[31m---------------------------------------------\e[0m";
	echo -e "\e[31m You need to provide commit string to check. \e[0m";
	echo -e "\e[31m $0 \"Commit message to check\" \e[0m";
	echo -e "\e[31m---------------------------------------------\e[0m";
	exit 1;
fi

MESSAGE="${1}"
while [ "$#" -gt 0 ]; do
    shift
	TMP="${1}"
	if [ -z "${TMP}" ]; then
echo "${MESSAGE}"| perl -ne 'if (m/^\[\w+-\d+\]\s\w*\s\([-\w\s\$\.\,\*\/]*\)\:.*$/i){exit 0;} elsif (m/^Merge (remote-tracking\s)?branch/){exit 0;} elsif (m/^(draft|wip):.*/i){exit 0;} else{exit 1;}'
        if [ "$?" != "0" ]; then
        	echo -e "\e[31m---------------------------------------------\e[0m";
        	echo -e "\e[31m COMMIT MESSAGE NOT MATCH RULE! \e[0m";
        	echo -e "\e[31m Message can match these rules:  \e[0m";
        	echo -e "\e[31m ^\[\w+-\d+\]\s\w*\s\([-\w\s\$\.\,\*\/]*\)\:.*$  \e[0m";
        	echo -e "\e[31m ^(draft|wip):.*  \e[0m";
			echo -e "\e[31m '^(draft|wip)?:?\s*\[\w+-\d+\]\s\w*\s\([-\w\s\$\.\,\*\/]*\)\:.*$'\e[0m";
		   	echo -e "\e[31m register independent \e[0m";
		   	echo -e "\e[31m https://kb.epam.com/display/EPMSM/Development#Development-%D0%A1%D1%82%D0%B8%D0%BB%D1%8C%D0%BA%D0%BE%D0%BC%D0%BC%D0%B8%D1%82%D0%BE%D0%B2 \e[0m";
			echo -e "\e[31m Like that: DRAFT|WIP [SOME-1234] some (SM): some text \e[0m";
        	echo -e "\e[31m Messase is: \"${MESSAGE}\" \e[0m";
        	echo -e "\e[31m---------------------------------------------\e[0m";
        	exit 1;
        fi
	else
		MESSAGE="$MESSAGE $TMP"
	fi
done
