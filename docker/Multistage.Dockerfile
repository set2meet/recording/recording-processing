# syntax=docker/dockerfile:experimental

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

#------------------------------------------
# test
#------------------------------------------
FROM alpine:3.12.0 AS test

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

RUN npm set unsafe-perm true

RUN apk add \
    nodejs \
    npm \
    openssh \
    build-base \
    libxtst-dev \
    gtk+3.0 \
    python2 \
    pkgconfig \
    git
# Setup ssh for getting dependencies from git.epam.com
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan git.epam.com >> ~/.ssh/known_hosts

WORKDIR /usr/recording-processing
COPY ./ ./

RUN --mount=type=ssh,id=default npm i && npm run build
#RUN --mount=type=ssh,id=default npx 'git+ssh://git@git.epam.com:epm-s2m/env-config.git' -- -module recording-processing -stand ${STAND} -path ./config/default.json -role_id ${ROLE_ID} -secret_id ${SECRET_ID}
RUN --mount=type=ssh,id=default git clone git@git.epam.com:epm-s2m/ffmpeg.git ffmpeg
RUN cp -v /usr/recording-processing/ffmpeg/bin/ffmpeg /usr/bin/ && chmod +x /usr/bin/ffmpeg && \
    cp -v /usr/recording-processing/ffmpeg/bin/ffprobe /usr/bin/ && chmod +x /usr/bin/ffprobe && \
    cp -v /usr/recording-processing/ffmpeg/bin/qt-faststart /usr/bin/ && chmod +x /usr/bin/qt-faststart


CMD ["/bin/sh", "-c", "npm run test; \
      if [ \"$?\" != \"0\" ]; then \
        cp -v junit.xml coverage-out/; \
        cp -r coverage/* coverage-out/ ;\
        exit 1; \
      else \
        cp -v junit.xml coverage-out/; \
        cp -r coverage/* coverage-out/ ; \
      fi"]


###########################################
# builder
###########################################
FROM alpine:3.12.0 AS builder

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

RUN npm set unsafe-perm true

RUN apk add \
    nodejs \
    npm \
    openssh \
    build-base \
    libxtst-dev \
    gtk+3.0 \
    python2 \
    pkgconfig \
    git
# Setup ssh for getting dependencies from git.epam.com
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan git.epam.com >> ~/.ssh/known_hosts

WORKDIR /usr/recording-processing
COPY ./ ./

RUN --mount=type=ssh,id=default npm i --production && npm run build
RUN --mount=type=ssh,id=default npx 'git+ssh://git@git.epam.com:epm-s2m/env-config.git' -- -module recording-processing -stand ${STAND} -path ./config/default.json -role_id ${ROLE_ID} -secret_id ${SECRET_ID}
RUN --mount=type=ssh,id=default git clone git@git.epam.com:epm-s2m/ffmpeg.git ffmpeg

###########################################
# recording-processing
###########################################
FROM alpine:3.12.0

RUN apk add \
    nodejs \
    npm
#    ffmpeg
COPY --from=builder /usr/recording-processing/ffmpeg/bin /usr/bin/
RUN chmod +x /usr/bin/ffmpeg && \
    chmod +x /usr/bin/ffprobe && \
    chmod +x /usr/bin/qt-faststart

# Install forever
RUN npm i -g forever

WORKDIR /usr/set2meet
# Copy Config and recording processing
COPY --from=builder /usr/recording-processing/config ./config/
COPY --from=builder /usr/recording-processing ./recording-processing

ENTRYPOINT ["forever", "recording-processing/dist/index.js"]
